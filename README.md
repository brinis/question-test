# Test
## Installation

## Symfony
* git clone https://gitlab.com/brinis/question-test
* docker-compose up -d
* php bin/console doctrine:fixtures:load
* Login: admin@gmail.com , password: admin
## Angular:
* cd client/
*  ng s -o
* modfication api_url dans environment selon votre  url local

## À propos

*iletaitunefoisundev* a été conçu initialement par [haykal Brinis](https://github.com/haykalBR). Si vous avez la moindre question, contactez [Haykal Brinis](mailto:haikelbrinis@gmail.com?subject=[quizs]%20project)
