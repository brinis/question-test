<?php

namespace App\Entity;

use App\Core\Traits\TimestampableTrait;
use App\Repository\RefQuestionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * @ORM\Entity(repositoryClass=RefQuestionRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("code")
 */
class RefQuestion
{
    use TimestampableTrait;
    public const MSA = 'MSA';
    public const MMA = 'MMA';
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int  $id;
    /**
     * @ORM\Column(type="string", length=100)
     */
    private string $name;
    /**
     * @ORM\Column(type="string", length=100,unique=true)
     */
    private string $code;

    /**
     * @ORM\OneToMany(targetEntity=Question::class, mappedBy="refQuestion")
     */
    private $questions;

    public function __construct()
    {
        $this->questions = new ArrayCollection();
    }
    public function __toString()
    {
      return $this->name;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    /**
     * @return Collection|Question[]
     */
    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    public function addQuestion(Question $question): self
    {
        if (!$this->questions->contains($question)) {
            $this->questions[] = $question;
            $question->setRefQuestion($this);
        }

        return $this;
    }

    public function removeQuestion(Question $question): self
    {
        if ($this->questions->removeElement($question)) {
            // set the owning side to null (unless already changed)
            if ($question->getRefQuestion() === $this) {
                $question->setRefQuestion(null);
            }
        }

        return $this;
    }


}
