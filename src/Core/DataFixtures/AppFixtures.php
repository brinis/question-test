<?php

namespace App\Core\DataFixtures;

use App\Entity\RefQuestion;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private UserPasswordHasherInterface $passwordHasher;
    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }
    public function load(ObjectManager $manager): void{
        $this->createUsers($manager);
        $this->createRefQuestion($manager);
    }
    private function createUsers(ObjectManager $manager):void{
        foreach ($this->getUserData() as [$password, $email]) {
            $user = new User();
            $user->setPassword($this->passwordHasher->hashPassword($user, $password));
            $user->setEmail((string) $email);
            $manager->persist($user);
        }
        $manager->flush();
    }
    private function getUserData(): array
    {
        return [
            ['haikel', 'haikelbrinis@gmail.com'],
            ['admin', 'admin@gmail.com'],
            [ 'haikel', 'jane_admin@symfony.com'],
            [ 'haikel', 'tom_admin@symfony.com'],
            [ 'haikel', 'john_user@symfony.com'],
        ];
    }
    private function createRefQuestion(ObjectManager $manager): void{
       $refQuestions = [
           ['code' => RefQuestion::MSA, 'name' => 'Multiple Choice Single Answer'],
           ['code' => RefQuestion::MMA, 'name' => 'Multiple Choice Multiple Answers'],
       ];
       foreach ($refQuestions as $refQuestion) {
           $refQuestionEntity = new RefQuestion();
           $refQuestionEntity->setName($refQuestion['name']);
           $refQuestionEntity->setCode($refQuestion['code']);
           $manager->persist($refQuestionEntity);
       }
       $manager->flush();

   }
}