<?php

namespace App\Controller;
use App\Entity\Question;
use App\Entity\RefQuestion;
use App\Repository\QuestionRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FinishQuestionAction extends AbstractController
{
    /**
     * @Route(
     *     methods={"POST"},
     *     name="question-save"
     * )
     */
    public function __invoke(Request $request,QuestionRepository $questionRepository):JsonResponse{
        $data = json_decode((string) $request->getContent(), true);

        try {
            $isSuccess=false;
            $totalScore=0;
            $errorScore=0;
            foreach ($data as $item){
              $question= $questionRepository->find($item['questionId']);
              if ($question && $question->getRefQuestion()){
                  $totalScore+=$question->getScore();
                      foreach($question->correctResponse() as $option){
                           if(!in_array($option->getId(), $item['response'], true)){
                               $errorScore+=$question->getScore();
                           }
                      }
              }
            }
            if(($totalScore/2)>$errorScore){
                $isSuccess=true;
            }
            return $this->json(['isSuccess' => $isSuccess], 200);
        } catch (\Exception $e) {
            return $this->json(['title' => 'Error during reset password', 'detail' => $e->getMessage()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        return $this->json(['title' => 'Error during finish question', 'detail' => $e->getMessage()], Response::HTTP_UNPROCESSABLE_ENTITY);

    }
}