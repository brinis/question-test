<?php

namespace App\Controller;

use App\Repository\QuestionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class StartQuestionAction extends AbstractController
{
    private QuestionRepository $questionRepository;

    public function __construct(QuestionRepository $questionRepository){

        $this->questionRepository = $questionRepository;
    }
    public function __invoke()
    {
        try{
            $questions = $this->questionRepository->random();
            $result=[];
             foreach ($questions as $question){
                 $options = [
                      'id'=>$question->getId(),
                      'label'=>$question->getLabel(),
                      'answer_type'=>$question->getRefQuestion()->getCode(),
                     ];
                foreach($question->getOptions() as $option){
                    $options['answers'][] = [
                        'id' => $option->getId(),
                        'body' => $option->getName(),
                    ];
                }
                 $result[] = $options;
             }
            return $this->json([
                'questions' => $result,
            ], 200);

        }catch(\Exception $e){
        return $this->json(['title' => 'Error during start question', 'detail' => $e->getMessage()], Response::HTTP_UNPROCESSABLE_ENTITY);
      }
    }
}