<?php

namespace App\Repository;

use App\Entity\Question;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Question|null find($id, $lockMode = null, $lockVersion = null)
 * @method Question|null findOneBy(array $criteria, array $orderBy = null)
 * @method Question[]    findAll()
 * @method Question[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuestionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Question::class);
    }
    public function random(){
        $qb = $this->createQueryBuilder('q');
        $id_limits = $qb
            ->select('MIN(q.id)', 'MAX(q.id)')
            ->getQuery()
            ->getOneOrNullResult();
        $random_possible_id = [];
        for($i=0;$i<3;$i++){
            $random_possible_id[]=random_int((int) $id_limits[1], (int) $id_limits[2]);
        }

        return $qb->select('q')
            ->where('q.id in (:random_id)')
            ->setParameter('random_id', $random_possible_id)
            ->setMaxResults(2)
            ->getQuery()
            ->getResult();
    }
}
