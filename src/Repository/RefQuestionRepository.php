<?php

namespace App\Repository;

use App\Entity\RefQuestion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RefQuestion|null find($id, $lockMode = null, $lockVersion = null)
 * @method RefQuestion|null findOneBy(array $criteria, array $orderBy = null)
 * @method RefQuestion[]    findAll()
 * @method RefQuestion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RefQuestionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RefQuestion::class);
    }

    // /**
    //  * @return RefQuestion[] Returns an array of RefQuestion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RefQuestion
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
