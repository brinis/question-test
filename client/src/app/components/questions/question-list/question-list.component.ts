import {Component, Input, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {AppDataState} from "../../../shared/state/AppDataState";
import {Questions} from "../../../shared/models/questions.model";
import {DataStateEnum} from "../../../shared/state/data.enum";

@Component({
  selector: 'app-question-list',
  templateUrl: './question-list.component.html',
  styleUrls: ['./question-list.component.scss']
})
export class QuestionListComponent implements OnInit {
  @Input() questionsInput$:Observable<AppDataState<Questions[]>> |null=null;
  readonly DataStateEnum=DataStateEnum;

  constructor() { }

  ngOnInit(): void {
  }

}
