import { Component, OnInit } from '@angular/core';
import {QuestionService} from "../../core/services/question.service";
import {Questions} from "../../shared/models/questions.model";
import {Observable} from "rxjs";
import {AppDataState} from "../../shared/state/AppDataState";
import {hng} from "../../shared/utils/hng";

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss']
})
export class QuestionsComponent implements OnInit {
  questionsT$:Observable<AppDataState<Questions[]>> |null=null;
  isSuccess:number = 0;
  constructor(private questionsService: QuestionService) { }
  ngOnInit(): void {
     localStorage.removeItem('questions') ;
    this.questionsT$= this.questionsService.getQuestions()
      .pipe(
        hng
      );

  }

  finish() {
    this.questionsService.save()
      .subscribe(data=>{
        if(data.isSuccess){
          this.isSuccess=1;
        }else{
          this.isSuccess=2;
        }
      })
  }
}
