import {Component, Input, OnInit} from '@angular/core';
import {Questions} from "../../../shared/models/questions.model";
import {QuestionService} from "../../../core/services/question.service";

@Component({
  selector: 'app-question-item',
  templateUrl: './question-item.component.html',
  styleUrls: ['./question-item.component.scss']
})
export class QuestionItemComponent implements OnInit {
  @Input() question:Questions|null=null;
  ansKey: any[] = [];

  constructor(private questionsService :QuestionService) { }

  ngOnInit(): void {

  }
  Answer(question: Questions, choice:any) {
    var answers = localStorage.getItem('questions');
    var obj=[{
      questionId:question.id,
      response:[choice]
    }]
    if (answers){
      var array=JSON.parse(answers);
      array.forEach((element: any) => {
        if (element.questionId == question.id){
          if(question.answer_type=="MMA"){
              if(element.response.includes(choice)){
                element.response= element.response.filter(function(f: any) { return f !== choice})
              }else{
                element.response=element.response.concat(choice)
              }
            }
          obj=[{
            questionId:question.id,
            response:element.response
          }]
        }else{
           obj = array.concat(obj)
        }
      });
    }

    localStorage.setItem('questions',JSON.stringify(obj))

  }
  }
