import { Injectable } from '@angular/core';
import {HttpClient, HttpEvent, HttpHeaders, HttpParams, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {environment} from '../../../environments/environment.prod';
import {catchError} from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  /**
   * @param http
   */
  constructor(private http: HttpClient) { }
  /**
   * @param error
   * @private
   */
  // tslint:disable-next-line:typedef
  private  formatErrors(error: any) {
    return  throwError({code: error.code, message: error.error});
  }
  /**
   * @param path
   * @param params
   */
  get(path: string, params: HttpParams = new HttpParams(),headers:HttpHeaders = new HttpHeaders()): Observable<any> {
    if (!headers.has('Content-Type')){
      headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    }
    return this.http.get<any>(`${environment.api_url}${path}`, { params,headers}).pipe(catchError(this.formatErrors));
  }
  /**
   * @param path
   * @param body
   * @param headers
   */
  post(path: string, body: any = {},headers: HttpHeaders =new HttpHeaders()): Observable<any> {
    headers = headers.set('content-type','application/json');
    return this.http.post(
      `${environment.api_url}${path}`,
      JSON.stringify(body),
      {headers}
    ).pipe(catchError(this.formatErrors));
  }
  // @ts-ignore
  /**
   * @param path
   * @param params
   */
  delete(path:string, params: HttpParams = new HttpParams()): Observable<any> {
    return this.http.delete(
      `${environment.api_url}${path}`, {params},
    ).pipe(catchError(this.formatErrors));
  }
  /**
   * @param path
   * @param body
   * @param headers
   */
  put(path: string, body: any = {},headers: HttpHeaders =new HttpHeaders()): Observable<any> {
    headers = headers.set('content-type','application/json; charset=utf-8');
    return this.http.put(
      `${environment.api_url}${path}`,
      JSON.stringify(body),
      {headers}
    ).pipe(catchError(this.formatErrors));
  }
  upload(path:string,file: File): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();
    formData.append('file', file);
    const request = new HttpRequest('POST', `${environment.api_url}${path}`, formData, {
      reportProgress: true,
      responseType: 'json'
    });
    return this.http.request(request);
  }
}
