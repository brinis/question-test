import { Injectable } from '@angular/core';
import {ApiService} from "./api.service";
import {Questions} from "../../shared/models/questions.model";
import {cast} from "../../shared/utils/cast";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class QuestionService {
  qns: any[]=[];
  qnProgress: number=1;
  constructor(private api : ApiService) { }
  getQuestions():Observable<Questions[]> {
    return this.api.get('/questions/start')
      .pipe(
        map(data=>{ return data['questions']}),
        cast(Questions)
     );
  }
  save():Observable<any>{
    var body=[];
    var answers = localStorage.getItem('questions');
    if(answers){
      body=JSON.parse(answers);
    }
    return this.api.post('/questions/finish',body);
  }
}
