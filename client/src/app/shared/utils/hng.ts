import { Observable, of } from 'rxjs';
import { catchError, map, startWith } from 'rxjs/operators';
import {DataStateEnum} from "../state/data.enum";
export function hng<T>(source: Observable<T>) {
  return source.pipe(
    map(data=>{
      return ({dataState:DataStateEnum.LOADED,data:data})
    }),
    startWith({dataState:DataStateEnum.LOADING}),
    catchError(err=>of({dataState:DataStateEnum.ERROR, errorMessage:err.message}))
  )

}
