import {Observable} from "rxjs";
import { map } from 'rxjs/operators';
export const cast = (output:any) => {
  // tslint:disable-next-line:only-arrow-functions
  return function<T>(source: Observable<T>) {
    return source.pipe(map(value=>{

      // @ts-ignore
      if(value.constructor === Array){
        const convertedValue = Array.from(value).map((obj)=>{
          return new output().deserialize(obj);
        });
        return convertedValue;
      }else {
        return new output().deserialize(value);
      }
    }))
  }
}
