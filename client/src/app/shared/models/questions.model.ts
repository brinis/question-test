import {QuestionsInterface} from "./questions.interface";
import {Deserializable} from "./Deserializable.model";
import {Answers} from "./answers.model";

export class Questions implements QuestionsInterface, Deserializable{
  id!:number;
  answer_type!:string;
  label!:string;
  answers!:Answers[];
  deserialize(input: any): this {
    Object.assign(this, input);
    if(input.answers!=undefined){
      // @ts-ignore
      this.answers = input.answers.map(answer => new Answers().deserialize(answer));
    }
    return this;
  }


}

