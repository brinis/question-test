import { AnswersInterface } from "./answers.interface";
import {Deserializable} from "./Deserializable.model";
export class Answers implements AnswersInterface , Deserializable{
  id!:number;
  body!:string;
  deserialize(input: any): this {
    Object.assign(this, input);
    return this;
  }
}
